# lib-maxiv-go-ver

*A lib that supports version comparisons*

```go
v1, _ := version.NewFromString("1")
v2, _ := version.NewFromString("1.0.1")
v3, _ := version.NewFromString("1.0.0")

v1.LesserThan(v2)         // true
v1.LesserThanOrEqual(v2)  // true
v1.GreaterThan(v2)        // false
v1.GreaterThanOrEqual(v2) // false
v1.LesserThanOrEqual(v3)  // true
v1.GreaterThanOrEqual(v3) // true
```
