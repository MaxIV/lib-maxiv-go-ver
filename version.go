package version

import (
	"fmt"
	"strconv"
	"strings"
)

type Version struct {
	major int
	minor int
	patch int
}

func New(major, minor, patch int) Version {
	return Version{major, minor, patch}
}

func NewFromString(s string) (Version, error) {
	var v Version

	parts := strings.Split(s, ".")

	// Major version
	majorString, parts := parts[0], parts[1:]
	major, err := strconv.Atoi(majorString)
	if err != nil {
		return v, fmt.Errorf("could not interpret '%s' as a number: %w", majorString, err)
	}

	v.major = major

	// Minor version
	if len(parts) < 1 {
		return v, nil
	}

	minorString, parts := parts[0], parts[1:]
	minor, err := strconv.Atoi(minorString)
	if err != nil {
		return v, fmt.Errorf("could not interpret '%s' as a number: %w", minorString, err)
	}

	v.minor = minor

	// Patch version
	if len(parts) < 1 {
		return v, nil
	}

	patchString, parts := parts[0], parts[1:]
	patch, err := strconv.Atoi(patchString)
	if err != nil {
		return v, fmt.Errorf("could not interpret '%s' as a number: %w", patchString, err)
	}

	v.patch = patch

	return v, nil
}

// String converts the version into a string
func (self Version) String() string {
	return fmt.Sprintf("%d.%d.%d", self.major, self.minor, self.patch)
}

// Major returns the major version
func (self Version) Major() int {
	return self.major
}

// Minor returns the minor version
func (self Version) Minor() int {
	return self.minor
}

// Patch returns the patch version
func (self Version) Patch() int {
	return self.patch
}

// GreaterThan compares the version with v
func (self Version) GreaterThan(v Version) bool {
	if self.major > v.major {
		return true
	}
	if self.minor > v.minor {
		return true
	}
	if self.patch > v.patch {
		return true
	}

	return false
}

// GreaterThanOrEqual compares with version v
func (self Version) GreaterThanOrEqual(v Version) bool {
	if self.GreaterThan(v) {
		return true
	}

	if self.major < v.major {
		return false
	}

	if self.minor < v.minor {
		return false
	}

	if self.patch < v.patch {
		return false
	}

	return true
}

// LesserThan compares the version with v
func (self Version) LesserThan(v Version) bool {
	if self.major < v.major {
		return true
	}
	if self.minor < v.minor {
		return true
	}
	if self.patch < v.patch {
		return true
	}

	return false
}

// LesserThanOrEqual compares with version v
func (self Version) LesserThanOrEqual(v Version) bool {
	if self.LesserThan(v) {
		return true
	}

	if self.major > v.major {
		return false
	}

	if self.minor > v.minor {
		return false
	}

	if self.patch > v.patch {
		return false
	}

	return true
}
