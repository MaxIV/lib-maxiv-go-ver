package version

import (
	"fmt"
	"testing"

	check "gitlab.com/MaxIV/lib-maxiv-go-check"
)

func TestVersionCreate(t *testing.T) {
	v, err := NewFromString("1")
	check.OK(t, err)
	check.Equals(t, 1, v.Major())
	check.Equals(t, 0, v.Minor())
	check.Equals(t, 0, v.Patch())

	v, err = NewFromString("1.1")
	check.OK(t, err)
	check.Equals(t, 1, v.Major())
	check.Equals(t, 1, v.Minor())
	check.Equals(t, 0, v.Patch())

	v, err = NewFromString("1.1.1")
	check.OK(t, err)
	check.Equals(t, 1, v.Major())
	check.Equals(t, 1, v.Minor())
	check.Equals(t, 1, v.Patch())

	_, err = NewFromString("")
	check.NotOK(t, err)

	_, err = NewFromString("a")
	check.NotOK(t, err)

	_, err = NewFromString("1.b")
	check.NotOK(t, err)

	_, err = NewFromString("1.0.c")
	check.NotOK(t, err)
}

func TestComparisons(t *testing.T) {
	// >
	check.Equals(t, false, Version{1, 0, 0}.GreaterThan(Version{2, 0, 0}))
	check.Equals(t, false, Version{2, 0, 0}.GreaterThan(Version{2, 1, 0}))
	check.Equals(t, false, Version{2, 2, 0}.GreaterThan(Version{2, 2, 1}))

	check.Equals(t, true, Version{0, 0, 1}.GreaterThan(Version{0, 0, 0}))
	check.Equals(t, true, Version{0, 1, 0}.GreaterThan(Version{0, 0, 9}))
	check.Equals(t, true, Version{1, 0, 0}.GreaterThan(Version{0, 9, 0}))

	// >=
	check.Equals(t, true, Version{1, 1, 0}.GreaterThanOrEqual(Version{1, 0, 0}))
	check.Equals(t, true, Version{1, 0, 1}.GreaterThanOrEqual(Version{1, 0, 0}))
	check.Equals(t, true, Version{1, 0, 0}.GreaterThanOrEqual(Version{1, 0, 0}))
	check.Equals(t, true, Version{1, 1, 1}.GreaterThanOrEqual(Version{1, 1, 0}))

	check.Equals(t, false, Version{1, 1, 0}.GreaterThanOrEqual(Version{1, 1, 1}))
	check.Equals(t, false, Version{1, 0, 1}.GreaterThanOrEqual(Version{1, 1, 1}))
	check.Equals(t, false, Version{0, 1, 1}.GreaterThanOrEqual(Version{1, 1, 1}))

	// <
	check.Equals(t, true, Version{1, 0, 0}.LesserThan(Version{2, 0, 0}))
	check.Equals(t, true, Version{2, 1, 0}.LesserThan(Version{2, 2, 0}))
	check.Equals(t, true, Version{2, 2, 1}.LesserThan(Version{2, 2, 2}))
	check.Equals(t, false, Version{2, 2, 2}.LesserThan(Version{2, 2, 2}))

	// <=
	check.Equals(t, true, Version{2, 2, 2}.LesserThanOrEqual(Version{2, 2, 3}))
	check.Equals(t, true, Version{1, 0, 0}.LesserThanOrEqual(Version{1, 0, 0}))
	check.Equals(t, true, Version{1, 1, 0}.LesserThanOrEqual(Version{1, 0, 1}))

	check.Equals(t, false, Version{1, 0, 2}.LesserThanOrEqual(Version{1, 0, 1}))
	check.Equals(t, false, Version{1, 1, 1}.LesserThanOrEqual(Version{1, 0, 1}))
	check.Equals(t, false, Version{2, 1, 1}.LesserThanOrEqual(Version{1, 1, 1}))
}

func TestString(t *testing.T) {
	check.Equals(t, "1.5.1248", fmt.Sprintf("%s", Version{1, 5, 1248}))
}
