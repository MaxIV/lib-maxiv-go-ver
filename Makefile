coverage:
	go test -tags test -coverprofile=coverage.out ./...;
	go tool cover -html=coverage.out;
	rm coverage.out;
